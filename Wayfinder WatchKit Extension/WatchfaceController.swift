//
//  InterfaceController.swift
//  Wayfinder WatchKit Extension
//
//  Created by Kevin Lieser on 08.09.22.
//

import WatchKit
import Foundation
import CoreLocation
import UIKit
import CoreMotion


class WatchfaceController: WKInterfaceController, WKCrownDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var compassScene: WKInterfaceSKScene!
    @IBOutlet weak var clockScene: WKInterfaceSKScene!
    var compassSceneSpriteKit: SKScene!
    var compassNode: SKNode!
    var compassNodeSn: SKSpriteNode?
    var compassNodeDeg: SKNode!
    var compassNodeDegSn: SKSpriteNode?
    var compassHeadingLastDegree: Double = 0
    var clockSceneSpriteKit: FaceScene!
    
    var graphicContext: CGContext?
    @IBOutlet weak var latlonTextCircleImage: WKInterfaceImage!
    
    var crownAccumulator = 0.0
    
    var locationManager: CLLocationManager!
    var altimeter: CMAltimeter!
    
    var lastLat: String = "0.000"
    var lastLon: String = "0.000"
    var lastTime: Date = Date.now
    var lastAltitude: Double = 0
    
    var language = "en"
    
    var colorSet = 0
    var colorSetData = [
        [ // Night mode
            "compassRing": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "compassLines": UIColor(red: 125/255, green: 0/255, blue: 10/255, alpha: 1),
            "compassDeg": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "gaugeBackground": UIColor(red: 58/255, green: 10/255, blue: 10/255, alpha: 1),
            "circleBackground": UIColor(red: 58/255, green: 10/255, blue: 10/255, alpha: 1),
            "ring1": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "ring2": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "ring3": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "date": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "dateday": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "altimeter": UIColor.black,
            "directionMarker": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "ringOuter": UIColor.black,
            "ringInner": UIColor(red: 58/255, green: 10/255, blue: 10/255, alpha: 1),
            "ringHours": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "ringMinutes": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "compassDegreeText": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "distanceValue": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
            "secondHand": UIColor(red: 255/255, green: 0/255, blue: 33/255, alpha: 1),
        ],
    ]
    var iconSetData = [
        [
            "iconAlt": UIImage(),//make image invisible
        ],
        [
            "iconAlt": UIImage(named: "AltitudeIconRed"),
        ],
    ]
    

    @IBOutlet weak var centerCompBottom: WKInterfaceImage!
    @IBOutlet weak var centerCompRight: WKInterfaceImage!
    @IBOutlet weak var centerCompLeft: WKInterfaceImage!
    @IBOutlet weak var compassDegreeText: WKInterfaceLabel!
    @IBOutlet weak var altimeterImage: WKInterfaceImage!
    @IBOutlet weak var altimeterText: WKInterfaceLabel!
    @IBOutlet weak var comp3Image: WKInterfaceImage!
    @IBOutlet weak var comp4Image: WKInterfaceImage!
    @IBOutlet weak var comp4ImageIcon: WKInterfaceImage!
    @IBOutlet weak var directionMarkerImage: WKInterfaceImage!
    @IBOutlet weak var ringOuterImage: WKInterfaceImage!
    @IBOutlet weak var ringInnerImage: WKInterfaceImage!
    @IBOutlet weak var ringHours: WKInterfaceImage!
    @IBOutlet weak var ringMinutes: WKInterfaceImage!
    
    @IBOutlet weak var imageAltmeter: WKInterfaceImage!
    
    var handSecond: SKSpriteNode?
    
    override func awake(withContext context: Any?) {
        
        self.clockSceneSpriteKit = FaceScene.init(fileNamed: "FaceScene")
        self.clockSceneSpriteKit.scaleMode = .aspectFit
        self.clockSceneSpriteKit.backgroundColor = UIColor.clear
        
        let faceNode = clockSceneSpriteKit.childNode(withName: "Face")
        let secondHand = faceNode!.childNode(withName: "Seconds")
        self.handSecond = secondHand as? SKSpriteNode
        
        self.handSecond!.colorBlendFactor = 1.0
        self.handSecond!.color = self.colorSetData[self.colorSet]["secondHand"]!
        
        clockScene.presentScene(self.clockSceneSpriteKit)
        
        self.compassSceneSpriteKit = SKScene.init(fileNamed: "Compass")
        compassSceneSpriteKit.backgroundColor = UIColor.clear
        compassSceneSpriteKit.scaleMode = .aspectFit
        
        self.compassNode = compassSceneSpriteKit.childNode(withName: "Compass")
        self.compassNodeSn = self.compassNode as? SKSpriteNode
        self.compassNodeDeg = compassSceneSpriteKit.childNode(withName: "CompassDeg")
        self.compassNodeDegSn = self.compassNodeDeg as? SKSpriteNode
        
        self.compassNodeSn!.colorBlendFactor = 1.0
        self.compassNodeDegSn!.colorBlendFactor = 1.0
        
        
        compassScene.presentScene(self.compassSceneSpriteKit)
        
        crownSequencer.delegate = self
        
        let app = Dynamic.PUICApplication.sharedPUICApplication()
        app._setStatusBarTimeHidden(true, animated: false, completion: nil)
        
        self.drawTextCircle()
        
        
        self.compassNodeSn!.color = self.colorSetData[self.colorSet]["compassLines"]!
        self.compassNodeDegSn!.color = self.colorSetData[self.colorSet]["compassDeg"]!
        
        self.comp4ImageIcon.setTintColor(self.colorSetData[self.colorSet]["directionMarker"]!)
        self.ringOuterImage.setTintColor(self.colorSetData[self.colorSet]["ringOuter"]!)
        self.ringInnerImage.setTintColor(self.colorSetData[self.colorSet]["ringInner"]!)
        self.ringHours.setTintColor(self.colorSetData[self.colorSet]["ringHours"]!)
        self.ringMinutes.setTintColor(self.colorSetData[self.colorSet]["ringMinutes"]!)
        self.compassDegreeText.setTextColor(self.colorSetData[self.colorSet]["compassDegreeText"]!)

        self.imageAltmeter.setImage(self.iconSetData[self.colorSet]["iconAlt"]!)
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse) {
            self.fetchLocationUpdate()
        } else {
            self.locationManager?.requestAlwaysAuthorization()
        }
        

        

        
        let dateTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) {  [weak self] _ in
            self!.fetchDate()
        }
        dateTimer.fire()

        //START test-start
        
        self.lastAltitude = 3200
        self.altimeterText.setText("\(Int(self.lastAltitude)) M")
        
        //END test-start

        self.altimeter = CMAltimeter()
        self.altimeter.startAbsoluteAltitudeUpdates(to: OperationQueue.main) {(data,error) in DispatchQueue.main.async {
            if let optData = data {
                self.lastAltitude = optData.altitude
                self.altimeterText.setText("\(Int(self.lastAltitude)) M")
            
                let size = CGSize(width: 210, height: 230)
                UIGraphicsBeginImageContextWithOptions(size, false, 0)
                self.graphicContext = UIGraphicsGetCurrentContext()
                self.graphicContext!.translateBy (x: size.width / 2, y: size.height / 2)
                self.graphicContext!.scaleBy(x: 1, y: -1)
                
                self.centreArcPerpendicular(text: "\(Int(self.lastAltitude)) M", context: self.graphicContext!, radius: 112, angle: (127.5 * .pi / 180), colour: self.colorSetData[self.colorSet]["altimeter"]!, font: UIFont(name: "RedHatMono-Bold", size: 14)!, clockwise: true)
                let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
                UIGraphicsEndImageContext()
                self.altimeterImage.setImage(returnImage)
            }
        } }
        
        

        let batteryTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) {  [weak self] _ in
            self!.fetchBattery()
        }
        batteryTimer.fire()

        
    }
    
    func fetchDate() {
        let customFormatter = DateFormatter()
        customFormatter.dateFormat = "eeeeee"
        
        let customFormatter2 = DateFormatter()
        customFormatter2.dateFormat = "d"
        
        var dateStr: String = customFormatter.string(for: Date.now)!
        dateStr = dateStr.replacingOccurrences(of: ".", with: "")
        if(dateStr.count > 3) {
            let index = dateStr.index(dateStr.startIndex, offsetBy:3)
            dateStr = String(dateStr[..<index])
        }

        //let sizeCenterCompB = CGSize(width: 42, height: 42)
        //let centerCompB = self.drawCenterText(size: sizeCenterCompB, color: (self.colorSetData[self.colorSet]["date"]!).cgColor, string: "\(customFormatter2.string(for: Date.now)!)", subcolor: (self.colorSetData[self.colorSet]["dateday"]!).cgColor, substring: "\(dateStr.uppercased())")
        //self.centerCompBottom.setImage(centerCompB)
    }
    
    func fetchBattery() {
        WKInterfaceDevice.current().isBatteryMonitoringEnabled = true
        var batteryLevel = CGFloat(WKInterfaceDevice.current().batteryLevel)
        
        //TODO add battery check. batteryLevel 0-1
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        crownSequencer.focus()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
    
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        
        self.crownAccumulator += rotationalDelta * 1.3
        if(Int(self.crownAccumulator) > (self.colorSetData.count - 1)) {
            self.crownAccumulator = 0
        }
        if(Int(self.crownAccumulator) < 0) {
            self.crownAccumulator = Double(self.colorSetData.count)
        }
        var setColorTheme = Int(self.crownAccumulator)
        if(self.colorSet != setColorTheme && setColorTheme >= 0 && setColorTheme < self.colorSetData.count) {
            self.colorSet = Int(self.crownAccumulator)
            self.updateColors()
        }
    }
    
    
    func updateColors() {
        self.compassNodeSn!.color = self.colorSetData[self.colorSet]["compassLines"]!
        self.compassNodeDegSn!.color = self.colorSetData[self.colorSet]["compassDeg"]!
        
        self.comp4ImageIcon.setTintColor(self.colorSetData[self.colorSet]["directionMarker"]!)
        self.ringOuterImage.setTintColor(self.colorSetData[self.colorSet]["ringOuter"]!)
        self.ringInnerImage.setTintColor(self.colorSetData[self.colorSet]["ringInner"]!)
        self.ringHours.setTintColor(self.colorSetData[self.colorSet]["ringHours"]!)
        self.ringMinutes.setTintColor(self.colorSetData[self.colorSet]["ringMinutes"]!)
        self.compassDegreeText.setTextColor(self.colorSetData[self.colorSet]["compassDegreeText"]!)
        
        
        self.handSecond!.color = self.colorSetData[self.colorSet]["secondHand"]!

        self.imageAltmeter.setImage(self.iconSetData[self.colorSet]["iconAlt"]!)
        
        self.drawTextCircle()
        self.fetchBattery()
        self.fetchDate()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways {
            self.fetchLocationUpdate()
        }
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.fetchLocationUpdate()
        }
    }
    
    func fetchLocationUpdate() {
        // self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.activityType = CLActivityType.otherNavigation
        self.locationManager.startUpdatingLocation()
        self.locationManager.startUpdatingHeading()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            var changed = false
            if(self.lastLat != location.latitude) {
                self.lastLat = location.latitude
                changed = true
            }
            if(self.lastLon != location.longitude) {
                self.lastLon = location.longitude
                changed = true
            }
            self.lastTime = location.timestamp
            if(changed) {
                self.drawTextCircle()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.compassDegreeText.setText("\(Int(newHeading.magneticHeading))°\(self.compassDirection(for: newHeading.magneticHeading)!)")
        /*
        if(abs(self.compassHeadingLastDegree - newHeading.magneticHeading) > 30) {
            self.compassNode.zRotation = (newHeading.magneticHeading * .pi / 180)
        } else {
            self.compassNode.run(SKAction.rotate(toAngle: (newHeading.magneticHeading * .pi / 180), duration: 0.05))
        }
        */
        self.compassNode.run(SKAction.rotate(toAngle: (newHeading.magneticHeading * .pi / 180), duration: 0.05))
        self.compassNodeDeg.run(SKAction.rotate(toAngle: (newHeading.magneticHeading * .pi / 180), duration: 0.05))
        self.compassHeadingLastDegree = newHeading.magneticHeading
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    
    func centreArcPerpendicular(text str: String, context: CGContext, radius r: CGFloat, angle theta: CGFloat, colour c: UIColor, font: UIFont, clockwise: Bool){
        // *******************************************************
        // This draws the String str around an arc of radius r,
        // with the text centred at polar angle theta
        // *******************************************************

        let characters: [String] = str.map { String($0) } // An array of single character strings, each character in str
        let l = characters.count
        let attributes = [NSAttributedString.Key.font: font]

        var arcs: [CGFloat] = [] // This will be the arcs subtended by each character
        var totalArc: CGFloat = 0 // ... and the total arc subtended by the string

        // Calculate the arc subtended by each letter and their total
        for i in 0 ..< l {
            arcs += [chordToArc(characters[i].size(withAttributes: attributes).width, radius: r)]
            totalArc += arcs[i]
        }

        // Are we writing clockwise (right way up at 12 o'clock, upside down at 6 o'clock)
        // or anti-clockwise (right way up at 6 o'clock)?
        let direction: CGFloat = clockwise ? -1 : 1
        let slantCorrection: CGFloat = clockwise ? -.pi / 2 : .pi / 2

        // The centre of the first character will then be at
        // thetaI = theta - totalArc / 2 + arcs[0] / 2
        // But we add the last term inside the loop
        var thetaI = theta - direction * totalArc / 2

        for i in 0 ..< l {
            thetaI += direction * arcs[i] / 2
            // Call centerText with each character in turn.
            // Remember to add +/-90º to the slantAngle otherwise
            // the characters will "stack" round the arc rather than "text flow"
            centre(text: characters[i], context: context, radius: r, angle: thetaI, colour: c, font: font, slantAngle: thetaI + slantCorrection)
            // The centre of the next character will then be at
            // thetaI = thetaI + arcs[i] / 2 + arcs[i + 1] / 2
            // but again we leave the last term to the start of the next loop...
            thetaI += direction * arcs[i] / 2
        }
    }

    func chordToArc(_ chord: CGFloat, radius: CGFloat) -> CGFloat {
        // *******************************************************
        // Simple geometry
        // *******************************************************
        return 2 * asin(chord / (2 * radius))
    }

    func centre(text str: String, context: CGContext, radius r: CGFloat, angle theta: CGFloat, colour c: UIColor, font: UIFont, slantAngle: CGFloat) {
        // *******************************************************
        // This draws the String str centred at the position
        // specified by the polar coordinates (r, theta)
        // i.e. the x= r * cos(theta) y= r * sin(theta)
        // and rotated by the angle slantAngle
        // *******************************************************

        // Set the text attributes
        let attributes = [NSAttributedString.Key.foregroundColor: c, NSAttributedString.Key.font: font]
        //let attributes = [NSForegroundColorAttributeName: c, NSFontAttributeName: font]
        // Save the context
        context.saveGState()
        // Undo the inversion of the Y-axis (or the text goes backwards!)
        context.scaleBy(x: 1, y: -1)
        // Move the origin to the centre of the text (negating the y-axis manually)
        context.translateBy(x: r * cos(theta), y: -(r * sin(theta)))
        // Rotate the coordinate system
        context.rotate(by: -slantAngle)
        // Calculate the width of the text
        let offset = str.size(withAttributes: attributes)
        // Move the origin by half the size of the text
        context.translateBy (x: -offset.width / 2, y: -offset.height / 2) // Move the origin to the centre of the text (negating the y-axis manually)
        // Draw the text
        str.draw(at: CGPoint(x: 0, y: 0), withAttributes: attributes)
        // Restore the context
        context.restoreGState()
    }

    
    func drawTextCircle() {
        print("X1")
        let size = CGSize(width: 180, height: 180)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.graphicContext = UIGraphicsGetCurrentContext()
        self.graphicContext!.translateBy (x: size.width / 2, y: size.height / 2)
        self.graphicContext!.scaleBy(x: 1, y: -1)
        
        var relativeDate = ""
        if(Date.now.timeIntervalSince1970 - self.lastTime.timeIntervalSince1970 < 30) {
            relativeDate = "GERADE AKTUALISIERT"
            if(self.language == "en") {
                relativeDate = "JUST UPDATED"
            }
        } else {
            let formatter = RelativeDateTimeFormatter()
            formatter.unitsStyle = .full
            formatter.dateTimeStyle = .named
            formatter.locale = Locale.current
            relativeDate = formatter.localizedString(for: self.lastTime, relativeTo: Date.now).uppercased()
        }
        
        /*
        for family in UIFont.familyNames.sorted() {
            let names = UIFont.fontNames(forFamilyName: family)
            print("Family: \(family) Font names: \(names)")
        }
        */
        
        var laengengrad = "LANGENGRAD"
        var breitengrad = "BREITENGRAD"
        if(self.language == "en") {
            laengengrad = "LONGITUDE"
            breitengrad = "LATITUDE"
        }
        
        
        var radius1: CGFloat = 68
        var radius2: CGFloat = 67.5
        var fontsize: CGFloat = 10
        if(WKInterfaceDevice.currentWatchModel == WatchModel.w44) {
            radius1 = 63
            radius2 = 62.5
            fontsize = 9
        } else if(WKInterfaceDevice.currentWatchModel == WatchModel.w41) {
            radius1 = 60
            radius2 = 60
            fontsize = 9
        } else if(WKInterfaceDevice.currentWatchModel == WatchModel.w40) {
            
        }
        
        self.centreArcPerpendicular(text: "\(breitengrad) \(self.lastLat)   \(laengengrad) \(self.lastLon)", context: self.graphicContext!, radius: radius1, angle: (90 * .pi / 180), colour: self.colorSetData[self.colorSet]["compassRing"]!, font: UIFont(name: "RedHatMono-Bold", size: fontsize)!, clockwise: true)
        self.centreArcPerpendicular(text: "\(relativeDate)", context: self.graphicContext!, radius: radius2, angle: (-90 * .pi / 180), colour: self.colorSetData[self.colorSet]["compassRing"]!, font: UIFont(name: "RedHatMono-Bold", size: fontsize)!, clockwise: false)
        
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.latlonTextCircleImage.setImage(returnImage)
    }
    
    
    func compassDirection(for heading: CLLocationDirection) -> String? {
        if heading < 0 { return nil }

        var directions = ["N", "NO", "O", "SE", "S", "SW", "W", "NW"]
        if(self.language == "en") {
            var directions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
        }
        let index = Int((heading + 22.5) / 45.0) & 7
        return directions[index]
    }
    

    func drawGaugeCorner(size: CGSize, color: CGColor, value: CGFloat, rotation: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.graphicContext = UIGraphicsGetCurrentContext()
        self.graphicContext!.translateBy (x: size.width / 2, y: size.height / 2)
        self.graphicContext!.scaleBy(x: 1, y: -1)
        
        let startAngle = -109° + (38° * value)
        let kThickness = CGFloat(6)
        self.graphicContext!.addArc(center: CGPoint(x: 0, y: 0),
                           radius: (110),
                           startAngle: startAngle - (rotation * .pi / 180),
                           endAngle: -109° - (rotation * .pi / 180),
                           clockwise: true)
        
        self.graphicContext!.setLineWidth(kThickness)
        self.graphicContext!.setLineCap(.round)
        self.graphicContext!.replacePathWithStrokedPath()
        let path = self.graphicContext!.path!
        self.graphicContext!.beginTransparencyLayer(auxiliaryInfo: nil)
        self.graphicContext!.saveGState()

        let rgb = CGColorSpaceCreateDeviceRGB()

        let gradient = CGGradient(
            colorsSpace: rgb,
            colors: [color, color] as CFArray,
            locations: [CGFloat(0), CGFloat(1)])!

        let bbox = path.boundingBox
        let startP = bbox.origin
        var endP = CGPoint(x: bbox.maxX, y: bbox.maxY);
        if (bbox.size.width > bbox.size.height) {
            endP.y = startP.y
        } else {
            endP.x = startP.x
        }

        self.graphicContext!.clip()
        
        self.graphicContext!.drawLinearGradient(gradient, start: startP, end: endP, options: CGGradientDrawingOptions(rawValue: 0))
        
        self.graphicContext!.restoreGState()
        self.graphicContext!.addPath(path)
        self.graphicContext!.setLineJoin(.miter)
        self.graphicContext!.endTransparencyLayer()
        
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return returnImage
    }
    
    func drawGaugeCornerDot(size: CGSize, color: CGColor, value: CGFloat, rotation: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.graphicContext = UIGraphicsGetCurrentContext()
        self.graphicContext!.translateBy (x: size.width / 2, y: size.height / 2)
        self.graphicContext!.scaleBy(x: 1, y: -1)
        
        let startAngle = -109° + (38° * value)
        let kThickness = CGFloat(10)
        let kLineWidth = CGFloat(2)
        self.graphicContext!.addArc(center: CGPoint(x: 0, y: 0),
                           radius: (110),
                           startAngle: startAngle - (rotation * .pi / 180),
                           endAngle: startAngle - (rotation * .pi / 180) - 0.1°,
                           clockwise: true)
        
        self.graphicContext!.setLineWidth(kThickness)
        self.graphicContext!.setLineCap(.round)
        self.graphicContext!.replacePathWithStrokedPath()
        let path = self.graphicContext!.path!
        self.graphicContext!.beginTransparencyLayer(auxiliaryInfo: nil)
        self.graphicContext!.saveGState()

        let rgb = CGColorSpaceCreateDeviceRGB()

        let gradient = CGGradient(
            colorsSpace: rgb,
            colors: [color, color] as CFArray,
            locations: [CGFloat(0), CGFloat(1)])!

        let bbox = path.boundingBox
        let startP = bbox.origin
        var endP = CGPoint(x: bbox.maxX, y: bbox.maxY);
        if (bbox.size.width > bbox.size.height) {
            endP.y = startP.y
        } else {
            endP.x = startP.x
        }

        self.graphicContext!.clip()
        
        self.graphicContext!.drawLinearGradient(gradient, start: startP, end: endP, options: CGGradientDrawingOptions(rawValue: 0))
        
        self.graphicContext!.restoreGState()
        self.graphicContext!.addPath(path)
        
        self.graphicContext!.setLineWidth(kLineWidth)
        UIColor.black.setStroke()
        self.graphicContext!.strokePath()
        
        self.graphicContext!.setLineJoin(.miter)
        self.graphicContext!.endTransparencyLayer()
        
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return returnImage
    }
    
    
    func drawCenterText(size: CGSize, color: CGColor, string: String, subcolor: CGColor, substring: String) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.graphicContext = UIGraphicsGetCurrentContext()
        
        self.graphicContext!.setFillColor((self.colorSetData[self.colorSet]["circleBackground"]!).cgColor)

        let rectangle = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        self.graphicContext!.addEllipse(in: rectangle)
        self.graphicContext!.drawPath(using: .fill)
        
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        let font = UIFont(name: "RedHatMono-Bold", size: 14)
        let string = NSAttributedString(string: string, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor(cgColor: color), NSAttributedString.Key.paragraphStyle : paragraphStyle])
        string.draw(in: CGRect(x: 0, y: 5, width: size.width, height: 30))
        
        let fontSm = UIFont(name: "RedHatMono-Bold", size: 10)
        let stringSm = NSAttributedString(string: substring, attributes: [NSAttributedString.Key.font: fontSm, NSAttributedString.Key.foregroundColor: UIColor(cgColor: subcolor), NSAttributedString.Key.paragraphStyle : paragraphStyle])
        stringSm.draw(in: CGRect(x: 0, y: 22, width: size.width, height: 30))
        
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return returnImage
    }
    
    
    func drawGaugeCenter(size: CGSize, color: CGColor, value: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.graphicContext = UIGraphicsGetCurrentContext()
        self.graphicContext!.translateBy (x: size.width / 2, y: size.height / 2)
        self.graphicContext!.scaleBy(x: 1, y: -1)
        
        let startAngle = 240°
        let endAngle = 240° - (300° * value)
        let kThickness = CGFloat(4)
        self.graphicContext!.addArc(center: CGPoint(x: 0, y: 0),
                           radius: (15),
                           startAngle: startAngle,
                           endAngle: endAngle,
                           clockwise: true)
        
        self.graphicContext!.setLineWidth(kThickness)
        self.graphicContext!.setLineCap(.round)
        self.graphicContext!.replacePathWithStrokedPath()
        let path = self.graphicContext!.path!
        self.graphicContext!.beginTransparencyLayer(auxiliaryInfo: nil)
        self.graphicContext!.saveGState()

        let rgb = CGColorSpaceCreateDeviceRGB()

        let gradient = CGGradient(
            colorsSpace: rgb,
            colors: [color, color] as CFArray,
            locations: [CGFloat(0), CGFloat(1)])!

        let bbox = path.boundingBox
        let startP = bbox.origin
        var endP = CGPoint(x: bbox.maxX, y: bbox.maxY);
        if (bbox.size.width > bbox.size.height) {
            endP.y = startP.y
        } else {
            endP.x = startP.x
        }

        self.graphicContext!.clip()
        
        self.graphicContext!.drawLinearGradient(gradient, start: startP, end: endP, options: CGGradientDrawingOptions(rawValue: 0))
        
        self.graphicContext!.restoreGState()
        self.graphicContext!.addPath(path)
        self.graphicContext!.setLineJoin(.miter)
        self.graphicContext!.endTransparencyLayer()
        
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return returnImage
    }
    
    
    
    func drawRing(size: CGSize, color: CGColor, value: CGFloat, radius: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.graphicContext = UIGraphicsGetCurrentContext()
        self.graphicContext!.translateBy (x: size.width / 2, y: size.height / 2)
        self.graphicContext!.scaleBy(x: 1, y: -1)
        
        let kThickness = CGFloat(5)
        self.graphicContext!.addArc(center: CGPoint(x: 0, y: 0),
                           radius: (radius),
                           startAngle: 90°,
                           endAngle: 90° - (360° * value),
                           clockwise: true)
        
        self.graphicContext!.setLineWidth(kThickness)
        self.graphicContext!.setLineCap(.round)
        self.graphicContext!.replacePathWithStrokedPath()
        let path = self.graphicContext!.path!
        
        
        self.graphicContext!.setShadow(
            offset: CGSize(width: 0, height: 1.5),
            blur: 1.5,
            color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        )
        
        
        self.graphicContext!.beginTransparencyLayer(auxiliaryInfo: nil)
        self.graphicContext!.saveGState()
        
        let rgb = CGColorSpaceCreateDeviceRGB()

        let gradient = CGGradient(
            colorsSpace: rgb,
            colors: [color, color] as CFArray,
            locations: [CGFloat(0), CGFloat(1)])!

        let bbox = path.boundingBox
        let startP = bbox.origin
        var endP = CGPoint(x: bbox.maxX, y: bbox.maxY);
        if (bbox.size.width > bbox.size.height) {
            endP.y = startP.y
        } else {
            endP.x = startP.x
        }

        self.graphicContext!.clip()
        
        self.graphicContext!.drawLinearGradient(gradient, start: startP, end: endP, options: CGGradientDrawingOptions(rawValue: 0))
        
        self.graphicContext!.restoreGState()
        self.graphicContext!.addPath(path)
        self.graphicContext!.setLineJoin(.miter)
        self.graphicContext!.endTransparencyLayer()
        
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return returnImage
    }
    
    
    
}


extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension BinaryFloatingPoint {
    var dms: (degrees: Int, minutes: Int, seconds: Int) {
        var seconds = Int(self * 3600)
        let degrees = seconds / 3600
        seconds = abs(seconds % 3600)
        return (degrees, seconds / 60, seconds % 60)
    }
}

extension CLLocation {
    var dms: String { latitude + " " + longitude }
    var latitude: String {
        let (degrees, minutes, seconds) = coordinate.latitude.dms
        return String(format: "%d°%d'%d\"%@", abs(degrees), minutes, seconds, degrees >= 0 ? " N" : " S")
    }
    var longitude: String {
        let (degrees, minutes, seconds) = coordinate.longitude.dms
        return String(format: "%d°%d'%d\"%@", abs(degrees), minutes, seconds, degrees >= 0 ? " O" : " W")
    }
}

postfix operator °

protocol IntegerInitializable: ExpressibleByIntegerLiteral {
  init (_: Int)
}

extension Double: IntegerInitializable {
  postfix public static func °(lhs: Double) -> CGFloat {
    return CGFloat(lhs) * .pi / 180
  }
}


enum WatchModel {
    case w38, w40, w41, w42, w44, w45, unknown
}


extension WKInterfaceDevice {

    static var currentWatchModel: WatchModel {
        switch WKInterfaceDevice.current().screenBounds.size {
        case CGSize(width: 136, height: 170):
            return .w38
        case CGSize(width: 162, height: 197):
            return .w40
        case CGSize(width: 176, height: 215):
            return .w41
        case CGSize(width: 156, height: 195):
            return .w42
        case CGSize(width: 184, height: 224):
            return .w44
        case CGSize(width: 198, height: 242):
            return .w45
        default:
            return .unknown
    }
  }
}


